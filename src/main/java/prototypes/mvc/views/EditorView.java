package prototypes.mvc.views;


import io.dropwizard.views.View;
import prototypes.mvc.models.Article;
import prototypes.mvc.views.base.IFormView;

public class EditorView extends View implements IFormView {

    private final Article article;

    public EditorView(Article article) {
        super("editor.mustache");
        this.article = article;
    }

    public Article getArticle() {
        return article;
    }

    @Override
    public String getFormAction() {
        return "/" + article.getId() + "/edit";
    }
}
