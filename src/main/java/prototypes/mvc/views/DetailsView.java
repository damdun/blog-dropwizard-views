package prototypes.mvc.views;


import io.dropwizard.views.View;
import prototypes.mvc.models.Article;

import java.util.List;

public class DetailsView extends View {

    private final Article article;

    public DetailsView(Article article) {
        super("details.mustache");
        this.article = article;
    }

    public Article getArticle() {
        return article;
    }
}
