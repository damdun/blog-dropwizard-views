package prototypes.mvc.views.base;

public interface IFormView {

    String getFormAction();

}
