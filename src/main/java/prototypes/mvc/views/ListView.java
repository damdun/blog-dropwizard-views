package prototypes.mvc.views;


import io.dropwizard.views.View;
import prototypes.mvc.models.Article;

import java.util.List;

public class ListView extends View {

    private final List<Article> articles;

    public ListView(List<Article> articles) {
        super("list.mustache");
        this.articles = articles;
    }

    public List<Article> getArticles() {
        return articles;
    }
}
