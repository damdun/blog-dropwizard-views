package prototypes.mvc.controllers;

import io.dropwizard.views.View;
import prototypes.Configuration;
import prototypes.db.ArticlesDatabase;
import prototypes.mvc.models.Article;
import prototypes.mvc.views.CreatorView;
import prototypes.mvc.views.DetailsView;
import prototypes.mvc.views.EditorView;
import prototypes.mvc.views.ListView;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.net.URI;

import static java.sql.DriverManager.getConnection;

@Path("/")
@Produces(MediaType.TEXT_HTML)
public class ApplicationController {

    private ArticlesDatabase database;

    public ApplicationController(Configuration config) {
        try {
            Class.forName(config.getDatabase().getDriver());
            database = new ArticlesDatabase(getConnection(config.getDatabase().getUrl()));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @GET
    public View showList() {
        return new ListView(database.getArticles());
    }

    @GET
    @Path("create")
    public View initCreate() {
        return new CreatorView(new Article());
    }

    @POST
    @Path("create")
    public Response doCreate(@FormParam("title") String title, @FormParam("text") String text) {
        database.insertArticle(new Article(title, text));
        return Response.seeOther(URI.create("/")).build();
    }

    @GET
    @Path("{id}")
    public View showDetails(@PathParam("id") Long id) {
        return new DetailsView(database.findArticle(id));
    }

    @GET
    @Path("{id}/edit")
    public View initEdit(@PathParam("id") Long id) {
        return new EditorView(database.findArticle(id));
    }

    @POST
    @Path("{id}/edit")
    public Response doEdit(@PathParam("id") Long id, @FormParam("title") String title, @FormParam("text") String text) {
        database.updateArticle(id, new Article(title, text));
        return Response.seeOther(URI.create("/")).build();
    }

    @DELETE
    @Path("{id}/delete")
    public View doDelete(@PathParam("id") Long id) {
        return null;
    }
}
