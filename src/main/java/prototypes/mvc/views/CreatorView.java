package prototypes.mvc.views;


import io.dropwizard.views.View;
import prototypes.mvc.models.Article;
import prototypes.mvc.views.base.IFormView;

public class CreatorView extends View implements IFormView {

    private final Article article;

    public CreatorView(Article article) {
        super("creator.mustache");
        this.article = article;
    }

    public Article getArticle() {
        return article;
    }

    @Override
    public String getFormAction() {
        return "/create";
    }
}
