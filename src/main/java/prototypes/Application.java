package prototypes;

import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import prototypes.mvc.controllers.ApplicationController;

public class Application extends io.dropwizard.Application<Configuration> {

    public static void main(String[] args) throws Exception {
        new Application().run(args);
    }

    @Override
    public void initialize(Bootstrap<Configuration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets"));
        bootstrap.addBundle(new ViewBundle<>());
    }

    @Override
    public void run(Configuration configuration, Environment environment) {
        environment.jersey().register(new ApplicationController(configuration));
    }

}
